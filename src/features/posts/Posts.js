import React, { useEffect, useState, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "react-bootstrap";
import { getPosts, data } from "./reducer";
import { Grid, Container, Button, Typography } from "@mui/material";
import "./css/style.css";
import img from "../../app/img/img_car.png";

const Posts = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const token = localStorage.getItem("token");
  const posts = useSelector(data);
  const dispatch = useDispatch();

  const fetchData = () => {
    dispatch(getPosts());
  };

  useEffect(() => {
    fetchData();
    setIsLoggedIn(!!token);
  }, [token]);

  // filter refrence by temen
  // const [filter, setFilter] = useState({
  //   tipeDriverInput: "",
  //   // date: "",
  //   timeValue: "",
  //   jumlahPenumpangInput: "",
  // });
  // const [Filter, setSubmitFilter] = useState({});

  // const handleSubmit = (p) => {
  //   p.preventDefault();
  //   setSubmitFilter(filter);
  // };

  // const handleChange = (p) => {
  //   setFilter({ ...filter, [p.target.name]: p.target.value });
  // };

  // const carFilter = useMemo(() => {
  //   return posts.filter((car) => {
  //     const tipeDriver = Filter.tipeDriverInput === "true";
  //     const user = Filter.jumlahPenumpangInput;
  //     const time = Filter.timeValue;
  //     const timeData = car.availableAt.split("T");
  //     const timeData2 = timeData[1].split(":");

  //     if (
  //       Filter.jumlahPenumpangInput &&
  //       Filter.tipeDriverInput &&
  //       Filter.timeValue
  //     ) {
  //       return (
  //         car.capacity >= user &&
  //         tipeDriver === car.available &&
  //         time >= timeData2[0]
  //       );
  //     }
  //     return true;
  //   });
  // }, [posts, Filter]);
  // filter refrence by temen

  return (
    <>
      <main style={{ width: "100%", height: "100%", marginBottom: "5%" }}>
        <section className="align-items-center bgheader">
          <div className="container kelang1">
            <div className="row">
              <div className="col-lg-6">
                <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                <p>
                  Selamat datang di Binar Rental. Kami menyediakan mobil
                  kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                  kebutuhanmu untuk sewa mobil selama 24 jam.
                </p>
                <div>
                  <a to="/" className="btn btn-success">
                    Mulai Sewa Mobil
                  </a>
                </div>
              </div>
              <div className="col-lg-6 kelang2">
                <img src={img} width="100%" alt="" />
              </div>
            </div>
          </div>
        </section>

        <Form
          // onSubmit={handleSubmit}
          // onChange={handleChange}
          className="card box"
        >
          <div className="boxcard">
            <div className="search-menu">
              <p>Type Driver</p>
              <Form.Select className="form-select" name="tipeDriverInput">
                <option selected value="0">
                  Pilih Tipe Driver
                </option>
                <option value="true">Dengan Sopir</option>
                <option value="false">Tanpa Sopir (Lepas Kunci)</option>
              </Form.Select>
            </div>
            <div className="search-menu">
              <p>Tanggal</p>
              <Form.Control
                type="date"
                className="form-control"
                placeholder="Pilih Tanggal"
                name="date"
              />
            </div>
            <div className="search-menu">
              <p>Waktu Jemput/Ambil</p>
              <Form.Select className="form-select" name="timeValue">
                <option selected value="0">
                  Pilih Waktu
                </option>
                <option value="08">08.00 AM</option>
                <option value="15">03.00 PM</option>
                <option value="20">08.00 PM</option>
              </Form.Select>
            </div>
            <div className="search-menu">
              <p>Jumlah Penumpang</p>
              <Form.Control
                type="text"
                className="form-control"
                placeholder="Jumlah Penumpang"
                name="jumlahPenumpangInput"
              />
            </div>
            <div className="search-menu">
              <div className="mid">
                <button type="submit" className="btn button-green lebar">
                  Cari Mobil
                </button>
              </div>
            </div>
          </div>
        </Form>

        {!isLoggedIn ? (
          <p>kamu tidak punya akses</p>
        ) : (
          <Container style={{ marginTop: "100px" }}>
            <Grid container spacing={3}>
              {posts?.data?.map((row, i) => (
                <Grid key={i} item xs={4}>
                  <div className="card-item">
                    <div className="foto">
                      <img
                        src={row?.image}
                        alt={row?.manufacture}
                        width="100%"
                      />
                    </div>
                    <div className="desc">
                      <h4 style={{ textAlign: "left" }}>
                        {row?.manufacture} / {row?.type}
                      </h4>
                      <h2 style={{ textAlign: "left", fontWeight: "700" }}>
                        Rp {row?.rentPerDay} / hari
                      </h2>
                      <p style={{ textAlign: "left" }}>{row?.description}</p>
                      <p style={{ textAlign: "left" }}>
                        {" "}
                        <i class="fa-solid fa-user-group"></i>
                        {row?.capacity}Orang
                      </p>
                      <p style={{ textAlign: "left" }}>
                        {" "}
                        <i class="fa-solid fa-gear"></i>
                        {row?.transmission}
                      </p>
                      <p style={{ textAlign: "left" }}>
                        <i class="fa fa-calendar" />
                        Tahun {row?.year}
                      </p>
                      {row?.availableAt}
                    </div>
                    <Button
                      variant="contained"
                      color="success"
                      // onClick={() => handleClick(row?.id)}
                    >
                      Pilih Mobil
                    </Button>
                  </div>
                </Grid>
              ))}
              {posts.status === "loading" && <Typography>Loading</Typography>}
            </Grid>
          </Container>
        )}
      </main>

      <footer className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-3">
              <p>
                Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000 <br />
                <br />
                binarcarrental@gmail.com
                <br />
                <br />
                081-233-334-808
                <br />
              </p>
            </div>

            <div className="col-lg-2 tinggi">
              <b>Our Service</b>
              <br />
              <b>Why us</b>
              <br />
              <b>Testimonial</b>
              <br />
              <b>Faq</b>
              <br />
            </div>

            <div className="col-lg-4">
              Connect with us
              <br />
              <br />
              <i className="fa-brands fa-facebook-f circle fa-2x"></i>
              <i className="fa-brands fa-instagram circle fa-2x"></i>
              <i className="fa-brands fa-twitter circle fa-2x"></i>
              <i className="fa-solid fa-envelope circle fa-2x"></i>
              <i className="fa-brands fa-twitch circle fa-2x"></i>
            </div>

            <div className="col-lg-3">
              Copyright Binar 2022
              <br />
              <br />
              <div className="boxfooter"></div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Posts;
