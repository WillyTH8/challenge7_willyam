import { useState, useEffect } from "react";
import Img from "../img/img_car.png";
import Img2 from "../img/img_service.png";
import Img3 from "../img/Ellipse 10 (1).png";
import "../fontawesome-free-6.0.0-web/css/all.min.css";
import "../fontawesome-free-6.0.0-web/css/fontawesome.min.css";
import "../css/style.css";
import "../../App.css";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const token = localStorage.getItem("token");

  useEffect(() => {
    setIsLoggedIn(!!token);
  }, [token]);

  return (
    <>
      {/* header1 */}
      <section className="align-items-center bgheader">
        <div class="container kelang1">
          <div class="row">
            <div class="col-lg-6">
              <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
              <p>
                Selamat datang di Binar Rental. Kami menyediakan mobil kualitas
                terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
              <div>
                {!isLoggedIn ? (
                  <a href="/login">
                    <button type="button" className="btn button-green">
                      Mulai Sewa Mobil
                    </button>
                  </a>
                ) : (
                  <a href="/posts">
                    <button type="button" className="btn button-green">
                      Mulai Sewa Mobil
                    </button>
                  </a>
                )}
              </div>
            </div>
            <div className="col-lg-6 kelang2">
              <img src={Img} width="100%" alt="" />
            </div>
          </div>
        </div>
      </section>

      {/* header2 */}
      <section className="align-items-center" id="header2">
        <div className="container pt-5">
          <div className="row align-items-center">
            <div className="col-md-6">
              <img src={Img2} width="100%" alt="" />
            </div>
            <div className="col-md-6">
              <h2 className="text-h2-2">
                Best Car Rental for any kind of trip in Palembang!
              </h2>
              <h6 className="text-h6-2">
                Sewa mobil di Surabaya bersama Binar Car Rental jaminan harga
                lebih murah dibandingkan yang lain, kondisi mobil baru, serta
                kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
                wedding, meeting, dll.
              </h6>
              <table>
                <tbody>
                  <tr>
                    <th style={{ height: "50px" }}>
                      <i
                        className="fa-solid fa-check circle-2"
                        style={{ alignItems: "center", display: "flex" }}
                      ></i>
                    </th>
                    <td>Sewa Mobil Dengan Supir di Bali 12 Jam</td>
                  </tr>
                  <tr>
                    <th style={{ height: "50px" }}>
                      <i
                        className="fa-solid fa-check circle-2"
                        style={{ alignItems: "center", display: "flex" }}
                      ></i>
                    </th>
                    <td>Sewa Mobil Lepas Kunci di Bali 24 Jam</td>
                  </tr>
                  <tr>
                    <th style={{ height: "50px" }}>
                      <i
                        className="fa-solid fa-check circle-2"
                        style={{ alignItems: "center", display: "flex" }}
                      ></i>
                    </th>
                    <td>Sewa Mobil Jangka Panjang Bulanan</td>
                  </tr>
                  <tr>
                    <th style={{ height: "50px" }}>
                      <i
                        className="fa-solid fa-check circle-2"
                        style={{ alignItems: "center", display: "flex" }}
                      ></i>
                    </th>
                    <td>Gratis Antar - Jemput Mobil di Bandara</td>
                  </tr>
                  <tr>
                    <th style={{ height: "50px" }}>
                      <i
                        className="fa-solid fa-check circle-2"
                        style={{ alignItems: "center", display: "flex" }}
                      ></i>
                    </th>
                    <td>Layanan Airport Transfer / Drop In Out</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>

      {/* whyus */}
      <section className="align-items-center py-5" id="whyus">
        <div className="container">
          <h2>Why Us?</h2>
          <p className="py-2">Mengapa harus pilih Binar Car Rental?</p>

          <div className="row">
            <div className="col-12 col-lg-3 d-flex responsif">
              <div className="card">
                <div className="card-body">
                  <i className="fa-solid fa-thumbs-up p-2"></i>
                  <h5 className="card-title">Mobil Lengkap</h5>
                  <p className="card-text">
                    Tersedia banyak pilihan mobil, kondisi masih baru, bersih
                    dan terawat
                  </p>
                </div>
              </div>
            </div>
            <div className="col-12 col-lg-3 d-flex responsif">
              <div className="card">
                <div className="card-body">
                  <i className="fa-solid fa-tag p-2"></i>
                  <h5 className="card-title">Harga Murah</h5>
                  <p className="card-text">
                    Harga murah dan bersaing, bisa bandingkan harga kami dengan
                    rental mobil lain
                  </p>
                </div>
              </div>
            </div>

            <div className="col-12 col-lg-3 d-flex responsif">
              <div className="card">
                <div className="card-body">
                  <i className="fa-solid fa-clock p-2"></i>
                  <h5 className="card-title">Layanan 24 Jam</h5>
                  <p className="card-text">
                    Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami
                    juga tersedia di akhir minggu
                  </p>
                </div>
              </div>
            </div>

            <div className="col-12 col-lg-3 d-flex responsif">
              <div className="card">
                <div className="card-body">
                  <i className="fa-solid fa-ribbon p-2"></i>
                  <h5 className="card-title">Sopir Profesional</h5>
                  <p className="card-text">
                    Sopir yang profesional, berpengalaman, jujur, ramah dan
                    selalu tepat waktu
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* Testimonial */}
      <div className="container-fluid pt-2" id="Testimonial">
        <div className="text-center pt-5">
          <h2>Testimonial</h2>
          <p className="pt-3">
            Berbagai review positif dari para pelanggan kami
          </p>
        </div>
      </div>

      <div className="container">
        <div
          id="carouselExampleControls"
          className="carousel slide"
          data-ride="carousel"
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="col-12 tengah" alt="First slide">
                <div className="card card-body slider-item">
                  <div className="row">
                    <div className="col-sm-2 pt-3 tengah">
                      <img src={Img3} alt="" />
                    </div>
                    <div className="col-sm-10">
                      <div className="pt-3">
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                      </div>
                      <div className="pt-3">
                        <p>
                          “Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do eiusmod lorem ipsum dolor sit amet,
                          consectetur adipiscing elit, sed do eiusmod lorem
                          ipsum dolor sit amet, consectetur adipiscing elit, sed
                          do eiusmod”
                        </p>
                        <strong>John Dee 32, Bromo</strong>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="carousel-item">
              <div className="col-12 tengah" alt=" Second slide">
                <div className="card card-body slider-item">
                  <div className="row">
                    <div className="col-sm-2 pt-3 tengah">
                      <img src="img/img_photo.png" alt="" />
                    </div>
                    <div className="col-sm-10">
                      <div className="pt-3">
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                      </div>
                      <div className="pt-3">
                        <p>
                          “Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do eiusmod lorem ipsum dolor sit amet,
                          consectetur adipiscing elit, sed do eiusmod lorem
                          ipsum dolor sit amet, consectetur adipiscing elit, sed
                          do eiusmod”
                        </p>
                        <strong>John Dee 32, Bromo</strong>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="carousel-item">
              <div className="col-12 tengah" alt="Third slide">
                <div className="card card-body slider-item">
                  <div className="row">
                    <div className="col-sm-2 pt-3 tengah">
                      <img src={Img3} alt="" />
                    </div>
                    <div className="col-sm-10">
                      <div className="pt-3">
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                      </div>
                      <div className="pt-3">
                        <p>
                          “Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do eiusmod lorem ipsum dolor sit amet,
                          consectetur adipiscing elit, sed do eiusmod lorem
                          ipsum dolor sit amet, consectetur adipiscing elit, sed
                          do eiusmod”
                        </p>
                        <strong>John Dee 32, Bromo</strong>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="carousel slider py-3">
        <div className="container">
          <div className="row">
            <div className="col-12 text-center mb-3">
              <a className="btn btn-secondary prev" type="button">
                <i className="fa-solid fa-solid fa-angle-left"></i>
              </a>
              <a className="btn btn-success next" type="button">
                <i className="fa-solid fa-solid fa-angle-right"></i>
              </a>
            </div>
          </div>
        </div>
      </section>

      {/* card */}
      <section className="py-5">
        <div className="container ">
          <div className="boxbar">
            <div className="row pt-5">
              <div className="col-lg-12 text-center pt-2">
                <h2>Sewa Mobil di (Lokasimu) Sekarang</h2>
                <p className="font">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div>
              <div className="col-12 text-center pt-3">
                <a href="#" className="btn btn-success">
                  Mulai Sewa Mobil
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* faq */}
      <section className="py-5 tengah" id="faq">
        <div className="container  row">
          <div className="col-12 col-lg-6">
            <h4>
              <b>Frequently Asked Question</b>
            </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>
          <div className="col-12 col-lg-6">
            <div className="accordion" id="accordionExample">
              <div className="mb-3">
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingOne">
                    <button
                      className="accordion-button"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseOne"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      Apa saja syarat yang dibutuhkan?
                    </button>
                  </h2>
                  <div
                    id="collapseOne"
                    className="accordion-collapse collapse show"
                    aria-labelledby="headingOne"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                      Delectus debitis ex, expedita accusantium impedit ipsum
                      at, quasi error laborum voluptas dolore possimus totam!
                      Illo sapiente et quos sint, saepe totam.
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingTwo">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseTwo"
                      aria-expanded="false"
                      aria-controls="collapseTwo"
                    >
                      Berapa hari minimal sewa mobil lepas kunci?
                    </button>
                  </h2>
                  <div
                    id="collapseTwo"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingTwo"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                      Delectus debitis ex, expedita accusantium impedit ipsum
                      at, quasi error laborum voluptas dolore possimus totam!
                      Illo sapiente et quos sint, saepe totam.
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingThree">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseThree"
                      aria-expanded="false"
                      aria-controls="collapseThree"
                    >
                      Berapa hari sebelumnya sebaiknya booking sewa mobil?
                    </button>
                  </h2>
                  <div
                    id="collapseThree"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingThree"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                      Delectus debitis ex, expedita accusantium impedit ipsum
                      at, quasi error laborum voluptas dolore possimus totam!
                      Illo sapiente et quos sint, saepe totam.
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingFour">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseFour"
                      aria-expanded="false"
                      aria-controls="collapseFour"
                    >
                      Apakah Ada biaya antar-jemput?
                    </button>
                  </h2>
                  <div
                    id="collapseFour"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingFour"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                      Delectus debitis ex, expedita accusantium impedit ipsum
                      at, quasi error laborum voluptas dolore possimus totam!
                      Illo sapiente et quos sint, saepe totam.
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-5">
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingFive">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseFive"
                      aria-expanded="false"
                      aria-controls="collapseFive"
                    >
                      Bagaimana jika terjadi kecelakaan
                    </button>
                  </h2>
                  <div
                    id="collapseFive"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingFive"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                      Delectus debitis ex, expedita accusantium impedit ipsum
                      at, quasi error laborum voluptas dolore possimus totam!
                      Illo sapiente et quos sint, saepe totam.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* footer */}
      <footer className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-3">
              <p>
                Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000 <br />
                <br />
                binarcarrental@gmail.com
                <br />
                <br />
                081-233-334-808
                <br />
              </p>
            </div>

            <div className="col-lg-2 tinggi">
              <b>Our Service</b>
              <br />
              <b>Why us</b>
              <br />
              <b>Testimonial</b>
              <br />
              <b>Faq</b>
              <br />
            </div>

            <div className="col-lg-4">
              Connect with us
              <br />
              <br />
              <i className="fa-brands fa-facebook-f circle fa-2x"></i>
              <i className="fa-brands fa-instagram circle fa-2x"></i>
              <i className="fa-brands fa-twitter circle fa-2x"></i>
              <i className="fa-solid fa-envelope circle fa-2x"></i>
              <i className="fa-brands fa-twitch circle fa-2x"></i>
            </div>

            <div className="col-lg-3">
              Copyright Binar 2022
              <br />
              <br />
              <div className="boxfooter"></div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}

export default App;
