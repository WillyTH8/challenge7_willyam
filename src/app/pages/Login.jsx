import React from "react";
import {
  TextField,
  Card,
  CardContent,
  Typography,
  Grid,
  Button,
  CardActions,
} from "@material-ui/core";
import { useGoogleLogin } from "@react-oauth/google";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const nav = useNavigate();
  const handleSuccess = (res) => {
    localStorage.setItem("token", res.credential);
    nav("/");
  };

  const login = useGoogleLogin({
    onSuccess: (tokenResponse) => handleSuccess(tokenResponse),
  });

  return (
    <div
      style={{
        width: "300px",
        margin: "auto",
        height: "80vh",
        display: "flex",
        alignItems: "center",
      }}
    >
      <Card>
        <CardContent>
          <Typography variant="h6">LOGIN</Typography>
          <Grid>
            <TextField label="Username" className="mt-3"></TextField>
          </Grid>
          <Grid>
            <TextField label="Password" className="mt-3"></TextField>
          </Grid>
        </CardContent>
        <CardActions>
          <Grid item xs={12}>
            <Grid>
              <Button>Sign in</Button>
            </Grid>
            <Grid>
              <Typography variant="caption">atau login dengan</Typography>
            </Grid>
            <Grid>
              {/* <GoogleOAuthProvider clientId="937763490070-scfba1na5j3qguqqh9po6rhu5o0kp63i.apps.googleusercontent.com">
                                <GoogleLogin 
                                onSuccess={(credentialResponse) =>{
                                    handleSuccess(credentialResponse);
                                }}
                                onError={() => {
                                    console.log("login failed");
                                }}
                                />
                            </GoogleOAuthProvider> */}
              <Button onClick={login}>Login with Google</Button>
            </Grid>
          </Grid>
        </CardActions>
      </Card>
    </div>
  );
};

export default Login;
