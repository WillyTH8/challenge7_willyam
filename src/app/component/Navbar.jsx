import { AppBar, Button, Box } from "@material-ui/core";
import { Container, padding } from "@mui/system";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";

const Navbar = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const token = localStorage.getItem("token");
  const nav = useNavigate();

  useEffect(() => {
    setIsLoggedIn(!!token);
  }, [token]);

  const handleClick = (path) => {
    if (path.tokenId) {
      localStorage.setItem("token", token);
      setIsLoggedIn(token);
    }
  };

  const handleLogout = () => {
    localStorage.removeItem("token");
    localStorage.clear();
    nav("/login");
  };

  return (
    <Container maxWidth="x1" className="navbar bgheader">
      <Box className="container" style={{ marginLeft: "auto" }}>
        <Button
          style={{ color: "black", display: "flex", float: "left" }}
          href="/"
        >
          Home
        </Button>

        {!isLoggedIn ? (
          <Button
            style={{ color: "black", display: "flex", float: "right" }}
            onClick={() => handleClick("/login")}
            href="/login"
          >
            Login
          </Button>
        ) : (
          <>
            <>
              <Button
                style={{ color: "black", display: "flex", float: "right" }}
                href="/#header2"
              >
                Our Service
              </Button>
            </>
            <Button
              style={{ color: "black", display: "flex", float: "right" }}
              href="/#whyus"
            >
              Why Us
            </Button>
            <Button
              style={{ color: "black", display: "flex", float: "right" }}
              href="/#Testimonial"
            >
              Testimonial
            </Button>
            <Button
              style={{ color: "black", display: "flex", float: "right" }}
              href="/#faq"
            >
              FAQ
            </Button>
            <Button
              className="btn btn-success"
              style={{ color: "black", display: "flex", float: "right" }}
              onClick={handleLogout}
            >
              Logout
            </Button>
          </>
        )}
      </Box>
    </Container>
  );
};

export default Navbar;
