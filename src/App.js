import React from "react";
import "./App.css";
import { Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import { GoogleOAuthProvider } from "@react-oauth/google";
import Landing from "./app/pages/Landing";
import List from "./features/posts/Posts";
// import Detail from "./features/posts/Detail";
import Login from "./app/pages/Login";
import Navbar from "./app/component/Navbar";

function App() {
  const menu = [
    { path: "/", component: <Landing /> },
    { path: "/posts", component: <List /> },
    // { path: "/posts/:id", component: <Detail /> },
    { path: "/login", component: <Login /> },
  ];

  return (
    <div className="App">
      <Navbar />
      <div class="App">
        <GoogleOAuthProvider clientId="937763490070-scfba1na5j3qguqqh9po6rhu5o0kp63i.apps.googleusercontent.com">
          <Routes>
            {menu.map((row, i) => (
              <Route exact path={row.path} element={row.component}></Route>
            ))}
          </Routes>
        </GoogleOAuthProvider>
      </div>
    </div>
  );
}

export default App;
